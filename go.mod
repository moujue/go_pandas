module gitee.com/quant1x/pandas

go 1.20

require (
	gitee.com/quant1x/data v0.7.3
	github.com/chewxy/math32 v1.10.1
	github.com/huandu/go-clone v1.4.1
	github.com/mymmsc/gox v1.3.3
	github.com/olekukonko/tablewriter v0.0.5
	github.com/qianlnk/pgbar v0.0.0-20210208085217-8c19b9f2477e
	github.com/shirou/gopsutil/v3 v3.23.1
	github.com/tealeg/xlsx v1.0.5
	github.com/tealeg/xlsx/v3 v3.2.4
	github.com/viterin/partial v1.0.0
	github.com/viterin/vek v0.4.0
	golang.org/x/exp v0.0.0-20220907003533-145caa8ea1d0
	gonum.org/v1/gonum v0.12.0
)

require (
	github.com/frankban/quicktest v1.11.2 // indirect
	github.com/go-ole/go-ole v1.2.6 // indirect
	github.com/google/btree v1.0.0 // indirect
	github.com/google/go-cmp v0.5.9 // indirect
	github.com/kr/pretty v0.2.1 // indirect
	github.com/kr/text v0.1.0 // indirect
	github.com/lufia/plan9stats v0.0.0-20211012122336-39d0f177ccd0 // indirect
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/peterbourgon/diskv v2.0.1+incompatible // indirect
	github.com/power-devops/perfstat v0.0.0-20210106213030-5aafc221ea8c // indirect
	github.com/qianlnk/to v0.0.0-20191230085244-91e712717368 // indirect
	github.com/rivo/uniseg v0.4.3 // indirect
	github.com/rogpeppe/fastuuid v1.2.0 // indirect
	github.com/shabbyrobe/xmlwriter v0.0.0-20200208144257-9fca06d00ffa // indirect
	github.com/tklauser/go-sysconf v0.3.11 // indirect
	github.com/tklauser/numcpus v0.6.0 // indirect
	github.com/yusufpapurcu/wmi v1.2.2 // indirect
	golang.org/x/sys v0.5.0 // indirect
	golang.org/x/text v0.5.0 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
)
